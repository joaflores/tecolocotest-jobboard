﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Models;
using FrontEnd.Service;
using Microsoft.AspNetCore.Mvc;

namespace FrontEnd.Controllers
{
    public class JobsController : Controller
    {

        private readonly JobService jobService;
        public JobsController(JobService jobService)
        {
            this.jobService = jobService;
        }
        public async Task<IActionResult> Index()
        {
            List<Job> jobs = await jobService.GetAll();

            return View(jobs);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Job model)
        {
            if (ModelState.IsValid)
            {
                
                await jobService.CreateJob(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public async Task<IActionResult> Edit(int id)
        {
            
            Job job = await jobService.Get(id);
            return View(job);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Job model)
        {
            if (ModelState.IsValid)
            {
                
                await jobService.EditJob(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public async Task<IActionResult> Delete(int id)
        {
            await jobService.DeleteJob(id);
            
                return RedirectToAction("Index");
            
        }

    }
}
