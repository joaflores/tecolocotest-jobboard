﻿using FrontEnd.Config;
using FrontEnd.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Service
{
    public class JobService
    {
        readonly String _backendApiBaseUrl;
        public JobService()
        {
            var setting = ConfigHelper.GetConfig();
            this._backendApiBaseUrl = setting["BackendApiBaseUrl"];
        }

        public async Task<List<Job>> GetAll()
        {
            List<Job> jobs = new List<Job>();
            using (var httpClient = new HttpClient())
            {
                using var response = await httpClient.GetAsync(this._backendApiBaseUrl + "/Jobs");
                string apiResponse = await response.Content.ReadAsStringAsync();
                jobs = JsonConvert.DeserializeObject<List<Job>>(apiResponse);
            }
            return jobs;
        }

        public async Task<Job> Get(int id)
        {
            Job job;
            using (var httpClient = new HttpClient())
            {
                
                using var response = await httpClient.GetAsync(this._backendApiBaseUrl + "/Jobs/" + id);
                string apiResponse = await response.Content.ReadAsStringAsync();
                job = JsonConvert.DeserializeObject<Job>(apiResponse);
            }
            return job;
        }

        public async Task<Job> CreateJob(Job job)
        {
            Job receivedJob;
            using var httpClient = new HttpClient();
           
            StringContent content = new StringContent(JsonConvert.SerializeObject(job), Encoding.UTF8, "application/json");

            using (var response = await httpClient.PostAsync(this._backendApiBaseUrl + "/Jobs", content))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                receivedJob = JsonConvert.DeserializeObject<Job>(apiResponse);
            }
            return receivedJob;
        }

        public async Task<Job> EditJob(Job job)
        {
            Job receivedJob;
            using var httpClient = new HttpClient();
            
            StringContent content = new StringContent(JsonConvert.SerializeObject(job), Encoding.UTF8, "application/json");

            using (var response = await httpClient.PutAsync(this._backendApiBaseUrl + "/Jobs/" + job.JobId,content))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                receivedJob = JsonConvert.DeserializeObject<Job>(apiResponse);
            }
            return receivedJob;
        }

        public async Task<Boolean> DeleteJob(int id)
        {
            using var httpClient = new HttpClient();
            
            using var response = await httpClient.DeleteAsync(this._backendApiBaseUrl + "/Jobs/" + id);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
