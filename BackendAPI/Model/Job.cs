﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Model
{
    public class Job
    {
        public int JobId { set; get; }
        public string JobTitle { set; get; }
        public string Description { set; get; }
        public DateTime CreatedAt { set; get; }
        public DateTime ExpiresAt { set; get; }
    }
}
