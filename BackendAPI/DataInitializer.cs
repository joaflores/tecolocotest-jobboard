﻿using BackendAPI.Context;
using BackendAPI.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI
{
    public class DataInitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new JobDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<JobDbContext>>()))
            {
                // Look for any jobs already in database.
                if (context.Jobs.Any())
                {
                    return;   // Database has been initilized
                }

                context.Jobs.AddRange(
                new Job
                {
                    JobTitle = "Enterprise Architect",
                    Description = "The Enterprise Architect's responsibilities include improving the current IT infrastructure, optimizing business operations, and setting the direction and approach for integrating information applications and programs. The Enterprise Architect is also responsible for cataloging, developing, coordinating, communicating, maintaining, and enforcing overall enterprise architecture models, representations, initiatives, capabilities, and components to adequately perform the organization's business and technology activities.",
                    CreatedAt = new DateTime(2020, 5, 30),
                    ExpiresAt = new DateTime(2020, 7, 30)
                },
                new Job
                {
                    JobTitle = "Solutions Architect",
                    Description = "The solution architect is responsible for the design of one or more applications or services within an organization, and is typically part of a solution development team. He or she must have a balanced mix of technical and business skills, and will often work with an enterprise architect for strategic direction.",
                    CreatedAt = new DateTime(2020, 5, 30),
                    ExpiresAt = new DateTime(2020, 7, 30)
                });
                context.SaveChanges();
            }
        }
    }
}
