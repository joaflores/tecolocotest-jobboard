﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using BackendAPI.Context;
using BackendAPI.Model;
using BackendAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackendAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class JobsController : ControllerBase
    {

        private readonly JobService jobService;

        public JobsController(JobService jobService)
        {
            this.jobService = jobService;
        }

        // GET api/Jobs
        [HttpGet]
        public IActionResult GetJobs()
        {
            List<Job> jobs= jobService.GetJobs();
            return Ok(jobs);
        }

        // GET api/Jobs/1
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetJob(int id)
        {
            Job job = jobService.GetJob(id);

            if (job == null)
            {
                return NotFound();
            }
            else {
                return Ok(job);
            }
            
        }

        //POST api/Jobs
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult PostJob (Job job)
        {

            Job newJob= jobService.CreateJob(job);
                            
            if (newJob != null)
            {
               
                return Ok(newJob);
            }
            else
            {
                return BadRequest();
            }
        }

        //PUT api/Job/1
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult PutJob(int id, [FromBody] Job job)
        {
            job = jobService.EditJob(id, job);
            
            if (job != null)
            {
                return Ok(job);
            }
            else
            {
                return NotFound();
            }
           
        }

        // DELETE: api/Job/2
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteJob(int id)
        {           
            if (jobService.RemoveJob(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }


    }
}
