﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackendAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class HelloController : Controller
    {
        // GET: HelloController
        public IActionResult Index()
        {
            return Ok("The API is working");
        }
    }
}
