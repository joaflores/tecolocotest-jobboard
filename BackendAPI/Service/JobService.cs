﻿using BackendAPI.Context;
using BackendAPI.Model;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Service
{
    public class JobService
    {
        private readonly JobDbContext jobDbContext;

        public JobService(JobDbContext jobDbContext)
        {
            this.jobDbContext = jobDbContext;
        }

        public Job GetJob(int jobId)
        {
            Job job = jobDbContext.Jobs.FirstOrDefault(a => a.JobId == jobId);
            return job;
        }

        public List<Job> GetJobs()
        {
            return jobDbContext.Jobs.ToList<Job>();
        }

        public Job CreateJob(Job job)
        {
            int addedJob = 0;
            EntityEntry entityEntry = jobDbContext.Jobs.Add(job);
            
            if ((Job)entityEntry.Entity == null)
            {
                job = null;
            }
            else
            {
                addedJob = jobDbContext.SaveChanges();
                if (addedJob < 1)
                {
                    job = null;
                }
            }

            return job;
        }

        public Job EditJob(int jobId, Job job)
        {
            int updatedJob = 0;
            Job jobRecord = jobDbContext.Jobs.FirstOrDefault(a => a.JobId == jobId);

            if (jobId > 0 && jobRecord != null)
            {
                job.JobId = jobId;
                jobRecord.JobTitle = job.JobTitle;
                jobRecord.Description = job.Description;
                jobRecord.CreatedAt = job.CreatedAt;
                jobRecord.ExpiresAt = job.ExpiresAt;

                EntityEntry entityEntry = jobDbContext.Jobs.Update(jobRecord);
                if ((Job)entityEntry.Entity == null)
                {
                    job = null;
                }
                else
                {
                    updatedJob = jobDbContext.SaveChanges();
                    if (updatedJob < 1)
                    {
                        job = null; 
                    }
                }
            }
            else
            {
                job = null;
            }

            return job;
        }

        public Boolean RemoveJob(int jobId)
        {
            int deletedJob = 0;
            Job jobRecord = jobDbContext.Jobs.FirstOrDefault(a => a.JobId == jobId);
            if (jobRecord != null)
            {
                EntityEntry entityEntry = jobDbContext.Jobs.Remove(jobRecord);
            }
            deletedJob = jobDbContext.SaveChanges();
            if (deletedJob > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
