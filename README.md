# TecolocoTest-JobBoard

This is a .NET Core test project for SAON Group

The solution contains 2 projects, the API (the backend) which contains all the business logic and data access and the Frontend which calls the API for the CRUD operations. Please note that the API project must be running and available to test any operation on the Frontend.


![](Diagram.png)

**Instructions**

1. git clone this repository.
2. From Visual Studio run the BackendAPI project. It is configured to run on localhost port 44378 (you can change this in launchSettings.json file inside the Properties directory).
3. Now run the FrontEnd project from Visual Studio. The FrontEnd looks for the backend on this URL: https://localhost:44378/api, this URL is configured in the appsettings.json file on the project directory. 